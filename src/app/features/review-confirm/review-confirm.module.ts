import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ReviewConfirmRoutingModule } from './review-confirm-routing.module';
import { ReviewConfirmComponent } from './review-confirm.component';
import { FormsModule } from '@angular/forms';
import { SharedModule } from 'src/app/shared/shared.module';


@NgModule({
  declarations: [ReviewConfirmComponent],
  imports: [
    CommonModule,
    ReviewConfirmRoutingModule,
    SharedModule,
    FormsModule,
  ]
})
export class ReviewConfirmModule { }


