import { Component, OnInit, AfterViewInit, ViewChild, ElementRef  } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Location } from '@angular/common';
import { Router, ActivatedRoute } from '@angular/router';

import Stepper from 'bs-stepper';
interface Months {
  value: string;
  viewValue: string;
}


@Component({
  selector: 'app-review-confirm',
  templateUrl: './review-confirm.component.html',
  styleUrls: ['./review-confirm.component.scss']
})
export class ReviewConfirmComponent implements OnInit {

  filingType = 0;
  @ViewChild('bsStepper', { static: false }) stepperElement!: ElementRef<any>;
  public stepper!: Stepper;

  months: Months[] = [
    { value: "01", viewValue: 'January' },
    { value: "02", viewValue: 'February' },
    { value: "03", viewValue: 'March' },
    { value: "04", viewValue: 'April' },
    { value: "05", viewValue: 'May' },
    { value: "06", viewValue: 'June' },
    { value: "07", viewValue: 'July' },
    { value: "08", viewValue: 'August' },
    { value: "09", viewValue: 'September' },
    { value: "10", viewValue: 'Octorber' },
    { value: "11", viewValue: 'November' },
    { value: "12", viewValue: 'December' },
  ]
  years: number[] = [];

  // Componen ใช้งาน
  month: Months | any;
  year: number | any;
  saleAmount: number | any = null;
  taxAmount: number | any = null;
  surchargeAmount: number | any = null;
  totalAmount : number | any = null;
  monthNow: string | any;
  yearNow = new Date().getFullYear();
  formGroup!: FormGroup;
  total: number | any = null;

  constructor( private localtion: Location,  private router: ActivatedRoute) {
    let month = new Date().getMonth() + 1;
    this.monthNow = month.toString()
    this.totalAmount = this.router.snapshot.paramMap.get('totalAmount')
    this.surchargeAmount = this.router.snapshot.paramMap.get('surchargeAmount')
    this.taxAmount = this.router.snapshot.paramMap.get('taxAmount')
    this.saleAmount = this.router.snapshot.paramMap.get('saleAmount')
    this.year = this.router.snapshot.paramMap.get('year')
    this.filingType = Number(this.router.snapshot.paramMap.get('filingType'))
    this.total = Number(this.saleAmount) + Number(this.taxAmount)
   }

  ngOnInit() {
    // this.saleAmount = 0
    const stepperElement = document.querySelector('#stepper1');
    if(stepperElement !== null){
      this.stepper = new Stepper(stepperElement , {
            linear: false,
            animation: true
          })
      }
  }

}
