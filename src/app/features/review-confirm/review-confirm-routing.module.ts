import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ReviewConfirmComponent } from './review-confirm.component';

const routes: Routes = [
  { path: '', component: ReviewConfirmComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReviewConfirmRoutingModule { }
