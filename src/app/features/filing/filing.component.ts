import { Component, OnInit, AfterViewInit, ViewChild, ElementRef  } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Location } from '@angular/common';
import { Router } from '@angular/router';

import Stepper from 'bs-stepper';
interface Months {
  value: string;
  viewValue: string;
}

@Component({
  selector: 'app-filing',
  templateUrl: './filing.component.html',
  styleUrls: ['./filing.component.scss']
})
export class FilingComponent implements OnInit {
  filingType = 0;
  @ViewChild('bsStepper', { static: false }) stepperElement!: ElementRef<any>;
  public stepper!: Stepper;

  months: Months[] = [
    { value: "01", viewValue: 'January' },
    { value: "02", viewValue: 'February' },
    { value: "03", viewValue: 'March' },
    { value: "04", viewValue: 'April' },
    { value: "05", viewValue: 'May' },
    { value: "06", viewValue: 'June' },
    { value: "07", viewValue: 'July' },
    { value: "08", viewValue: 'August' },
    { value: "09", viewValue: 'September' },
    { value: "10", viewValue: 'Octorber' },
    { value: "11", viewValue: 'November' },
    { value: "12", viewValue: 'December' },
  ]
  years: number[] = [];

  // Componen ใช้งาน
  month: Months | any;
  year: number | any;
  saleAmount: number | any = null;
  taxAmount: number | any = null;
  surchargeAmount: number | any = null;
  totalAmount : number | any = null;
  monthNow: string | any;
  yearNow = new Date().getFullYear();
  formGroup!: FormGroup;
  penaltyAmount: number | any = null;

  constructor( private localtion: Location,  private router: Router) {
    let month = new Date().getMonth() + 1;
    this.monthNow = month.toString()
   }

  ngOnInit() {

    const stepperElement = document.querySelector('#stepper1');
    console.log(stepperElement);
    if(stepperElement !== null){
      this.stepper = new Stepper(stepperElement , {
            linear: false,
            animation: true
          })
      }
      this.plusYear();
  }

  next() {
    this.stepper.next();
  }

  onSubmit() {
    return false;
  }

  plusYear() {
    let limityear = 2020;
    let year = new Date().getFullYear();
    let diff = year - limityear;
    diff += 1; // 2020
    for (let i = 0; i < diff; i++) {
      this.years.push(2020 + i);
    }
  }

  onFilingTypeChange(event: any) {
    console.log(event.target.value);
    this.filingType = Number(event.target.value);
  }

  onMonthChange(event: any) {
    this.month = event.target.value;
    console.log(this.month);

  }

  onchangeYear(event: any) {
    this.year = event.target.value

  }

  onChangeSaleAmount() {
    this.taxAmount = Number(this.saleAmount) * 0.07;
    this.surchargeAmount = Number(this.taxAmount) * 0.1
    this.totalAmount = Number(this.taxAmount) + Number(this.surchargeAmount) + 200.00

  }

  backPage() {
    this.localtion.back();
  }

  validateForm(data: number) {
    console.log(data);

    if (this.totalAmount && this.surchargeAmount && this.taxAmount && this.saleAmount && this.month) {
      this.nextPage();
    } else {
      alert("กรุณากรอกข้อมูลให้ครบถ้วน");
    }
  }

  nextPage() {
    this.router.navigate(['/review', {totalAmount:this.totalAmount,surchargeAmount: this.surchargeAmount,taxAmount: this.taxAmount ,saleAmount: this.saleAmount,month: this.month, year: this.year, filingType: this.filingType}]);
  }

  backStep() {
    this.filingType = 0
  }

  nextStep() {
    this.filingType = 1
  }

}
