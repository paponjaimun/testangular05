import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FilingRoutingModule } from './filing-routing.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { FormsModule } from '@angular/forms';
import { FilingComponent } from '../filing/filing.component';

@NgModule({
  declarations: [FilingComponent],
  imports: [
    CommonModule,
    FilingRoutingModule,
    SharedModule,
    FormsModule,
  ]
})
export class FilingModule { }


