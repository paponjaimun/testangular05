import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MainInputRoutingModule } from './main-input-routing.module';
import { FilingComponent } from '../filing/filing.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
// import { ReviewConfirmComponent } from './components/review-confirm/review-confirm.component';
// import { RouterModule, Routes } from '@angular/router';

@NgModule({
  declarations: [
    // FilingTypeComponent,
    // ReviewConfirmComponent,
    // FilingComponent
  ],
  imports: [
    CommonModule,
    MainInputRoutingModule,
    SharedModule,
    FormsModule,
    // BrowserModule
  ]
})
export class MainInputModule { }
