import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MainInputComponent } from './main-input.component';

const routes: Routes = [
  // { path: '', component: FilingTypeComponent },
  // { path: '/review', component: ReviewConfirmComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MainInputRoutingModule { }
