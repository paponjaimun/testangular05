import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MainLayoutComponent } from './core/main-layout/main-layout.component';

const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: 'filing' },
  {
    path: 'filing',
    component: MainLayoutComponent,
    loadChildren: () =>
      import('./features/filing/filing.module').then((m) => m.FilingModule)
  },
  {
    path: 'review',
    component: MainLayoutComponent,
    loadChildren: () =>
      import('./features/review-confirm/review-confirm.module').then((m) => m.ReviewConfirmModule)
  },
  {
    path: '**',
    redirectTo: '/error/404'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
