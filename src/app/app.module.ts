import { CoreModule } from './core/core.module';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NgModule } from '@angular/core';
// import { FilingTypeComponent } from './features/filing-type/filing-type.component';
// import { FilingComponent } from './features/filing/filing.component';
// import { MainInputComponent } from './features/main-input/main-input.component';

@NgModule({
  declarations: [
    AppComponent,
    // FilingComponent,
    // MainInputComponent
  ],
  imports: [
    CoreModule, AppRoutingModule
  ],

  bootstrap: [AppComponent]
})
export class AppModule { }

